<section class="content">
          <div class="row">


          <div class="col-md-12">

            <?php

            //var_dump($update_manager);
            ?>

            <?php

            if ($this->session->flashdata('flsh_msg')!=null) {

             ?>
                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4>	<i class="icon fa fa-check"></i> Berhasil!</h4>
                                <?php echo $this->session->flashdata('flsh_msg'); ?>
                              </div>
            <?php
            }
            ?>

            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Request Order</h3>
                  <div class="box-tools">
                    <?php
                    if(($this->session->userdata('is_role_user') == 2113) || ($this->session->userdata('is_role_user') == 2114) || ($this->session->userdata('is_role_user') == 2222)){
                        }else{

                    ?>
                      <a href="/admin/ro/new_request">

                        <button class="btn btn-sm btn-block btn-success"><i class="fa fa-plus fa-align-left"> </i>  Request Order</button>
                      </a>
                        <?php
                      }
                      ?>


                    </div>
                </div><!-- /.box-header -->


                <div class="box-body no-padding">

                  <?php //var_dump($data_mhs); ?>

                  <table class="table table-striped">
                    <tbody><tr>
                      <th style="width: 10px">#</th>
                      <th>No PO</th>
                      <th>No RO</th>
                      <th>Material</th>
                      <th>Date Created</th>

                      <th>Qty</th>
                      <th>Price</th>
                      <th>Total Price</th>
                      <th>Status</th>
                      <th>Action</th>

                    </tr>

                    <?php
                      $no = 1;
                      //var_dump($data_list_user);
                      foreach ($data_list_po as $key => $value) {
                    ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td>-</td>
                      <td><?php echo $value->no_ro; ?></td>
                      <td><?php echo $value->material_name; ?></td>
                      <td><?php echo $value->date_created; ?></td>
                      <td><?php echo $value->qty; ?></td>
                      <td><?php echo $value->price; ?></td>
                      <td><?php echo $value->total; ?></td>
                      <td>
                        <?php
                        if($value->status == 0){
                          echo '<span class="badge bg-red">request</span>';

                        }elseif($value->status == 1){
                            echo '<span class="badge bg-yellow">pending manager</span>';

                        }elseif($value->status == 2){
                          echo '<span class="badge bg-green">approved</span>';
                        }elseif($value->status == 3){
                          echo '<span class="badge bg-blue">purchase order</span>';
                        }


                       ?></td>


                      <td>

                        <div class="btn-group">
                          <button type="button" class="btn btn-success btn-flat btn-xs">Action</button>
                          <button type="button" class="btn btn-success btn-flat dropdown-toggle btn-xs" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <ul class="dropdown-menu" role="menu">
                            <?php
                            if($this->session->userdata('is_role_user') == 2114){

                            ?>
                              <li><a href="/admin/ro/<?php echo $value->po_id; ?>/approvem">Approve</a></li>
                            <?php
                            }elseif ($this->session->userdata('is_role_user') == 2113) {


                            ?>
                              <li><a href="/admin/ro/<?php echo $value->po_id; ?>/approved">Approve</a></li>
                            <?php
                            }
                            ?>
                            <li><a href="/admin/ro/<?php echo $value->po_id; ?>">Lihat data</a></li>
                            <li><a href="/admin/ro/<?php echo $value->po_id; ?>/delete">Delete Data</a></li>

                            <?php
                            if($value->status == 2){

                            ?>
                              <li><a href="/admin/ro/<?php echo $value->po_id; ?>/createpo">Create PO</a></li>
                            <?php
                            }
                            ?>
                          </ul>
                        </div>

                      </td>
                    </tr>

                    <?php

                    }

                    ?>

                  </tbody></table>
                </div><!-- /.box-body -->

                <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-left">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                  </ul>
                </div>

              </div>
          </div>


          <!-- Your Page Content Here -->
          </div>



        </section>
