<section class="content">
          <div class="row">


          <div class="col-md-12">


<div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Detail Request Order Approved</h3>
                </div>

                <!-- /.box-header -->
                <!-- form start -->
                  <?php
                  //var_dump($detail_ro);
                  foreach ($detail_ro as $key => $value) {

                  ?>
                  <div class="box-body form-horizontal">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">No Request Order</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <?php echo $value->no_ro;?></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Material Name</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <?php echo $value->material_name;?></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Date Created</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <?php echo $value->date_created;?></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Company Name</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <?php echo $value->company_name;?></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Supplier Name</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <?php echo $value->supplier_name;?></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Qty</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <?php echo $value->qty;?></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Price</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <i>Rp. <?php echo $value->price;?>,-</i></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Total</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">: <i>Rp. <?php echo $value->total;?>,-</i></label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                      <div class="col-sm-10">
                        <label  class="col-sm-0 control-label">:
                          <?php

                          if($value->status == 0){
                            echo '<span class="badge bg-red">request</span>';

                          }elseif($value->status == 1){
                              echo '<span class="badge bg-yellow">pending manager</span>';

                          }else {
                            echo '<span class="badge bg-green">approved</span>';
                          }


                         ?></label>
                      </div>
                    </div>


                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Create PO</button>
                  </div><!-- /.box-footer -->
                  <?php } ?>
              </div>
          </div>
        </div>
      </section>
