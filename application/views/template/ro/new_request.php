
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          <div class="row">


          <div class="col-md-6">
            <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">New Request Order</h3>
                    <?php
                      //var_dump($data_material);

                    ?>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <?php echo form_open('admin/ro/save', ['class' => 'form-signin', 'role' => 'form']); ?>

                      <!-- text input -->
                      <div class="form-group">
                        <label>No Request</label>
                        <input type="text" class="form-control" placeholder="no_ro" name="no_ro">
                      </div>
                      <div class="form-group">
                        <label>Material</label>
                        <select class="form-control" name="material">
                          <option value="">-- Choose Material --</option>

                          <?php foreach ($data_material_supplier as $key => $value): ?>
                            <option value="<?php echo $value->supplier_id;?>"><?php echo $value->material_name." ( ".$value->color_name." ) - ".$value->supplier_name;?></option>
                          <?php endforeach; ?>

                        </select>
                      </div>

                      <div class="form-group">
                        <label>Date Created</label>
                        <input type="text" class="form-control" placeholder="date_created" name="date_created" value="<?php echo date("Y-m-d h:i:s");?>">
                      </div>

                      <div class="form-group">
                        <label>Company Name</label>
                        <select class="form-control" name="company_id">

                          <?php foreach ($data_company as $key => $value): ?>
                            <option value="<?php echo $value->company_id;?>"><?php echo $value->company_name;?></option>
                          <?php endforeach; ?>

                        </select>
                      </div>


                      <div class="form-group">
                        <label>Qty</label>
                        <input type="text" class="form-control" placeholder="qty" name="qty">
                      </div>

                      <div class="form-group">
                        <button class="btn btn-block btn-default">Reset</button>
                        <button class="btn btn-block btn-success">Request</button>
                      </div>

                    </form>
                    <?php echo form_close(); ?>
                  </div><!-- /.box-body -->
                </div>
          </div>


          <!-- Your Page Content Here -->
          </div>


        </section><!-- /.content -->
