<section class="content">
          <div class="row">


          <div class="col-md-12">

            <?php

            if ($this->session->flashdata('flsh_msg')!=null) {

             ?>
                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4>	<i class="icon fa fa-check"></i> Berhasil!</h4>
                                <?php echo $this->session->flashdata('flsh_msg'); ?>
                              </div>
            <?php
            }
            ?>

            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Material</h3>
                  <div class="box-tools">
                      <a href="#">
                        <button class="btn btn-sm btn-block btn-success"><i class="fa fa-plus fa-align-left"> </i>  Add Material</button>
                      </a>
                    </div>
                </div><!-- /.box-header -->


                <div class="box-body no-padding">

                  <?php //var_dump($data_mhs); ?>

                  <table class="table table-striped">
                    <tbody><tr>
                      <th style="width: 10px">#</th>

                      <th>Id</th>
                      <th>Material Name</th>
                      <th>Color Name</th>
                      <th>Size</th>
                      <th>Price</th>
                      <th>Action</th>
                    </tr>

                    <?php
                      $no = 1;
                      //var_dump($data_material);
                      foreach ($data_material as $key => $value) {
                    ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $value->material_id; ?></td>
                      <td><?php echo $value->material_name; ?></td>
                      <td><?php echo $value->color_name; ?></td>
                      <td><?php echo $value->size; ?></td>
                      <td><?php echo $value->price; ?></td>

                      <td>
                          <a href="#">
                            <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-list-alt"></i> View</button>
                          </a>
                          <a href="#">
                          <button type="button" class="btn btn-xs btn-danger btn-flat" onclick="ConfirmDelete()"><i class="fa fa-trash"></i> Delete</button>
                          </a>
                          <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-edit"></i> Edit</button>
                      </td>
                    </tr>

                    <?php

                    }

                    ?>

                  </tbody></table>
                </div><!-- /.box-body -->
              </div>
          </div>


          <!-- Your Page Content Here -->
          </div>



        </section>
