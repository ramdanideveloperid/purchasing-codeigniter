<section class="content">
          <div class="row">


          <div class="col-md-12">

            <?php

            if ($this->session->flashdata('flsh_msg')!=null) {

             ?>
                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4>	<i class="icon fa fa-check"></i> Berhasil!</h4>
                                <?php echo $this->session->flashdata('flsh_msg'); ?>
                              </div>
            <?php
            }
            ?>

            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Color</h3>
                  <div class="box-tools">
                      <a href="/admin/mahasiswa/add">
                        <button class="btn btn-sm btn-block btn-success"><i class="fa fa-plus fa-align-left"> </i>  Add Color</button>
                      </a>
                    </div>
                </div><!-- /.box-header -->


                <div class="box-body no-padding">

                  <?php //var_dump($data_mhs); ?>

                  <table class="table table-striped">
                    <tbody><tr>
                      <th style="width: 10px">#</th>

                      <th>Color Code</th>
                      <th>Color Name</th>

                      <th>Action</th>
                    </tr>

                    <?php
                      $no = 1;
                      foreach ($data_warna as $key => $value) {
                    ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $value->color_code; ?></td>
                      <td><?php echo $value->color_name; ?></td>

                      <td>
                          <a href="mahasiswa/<?php echo $value->color_id; ?>">
                            <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-list-alt"></i> View</button>
                          </a>
                          <a href="mahasiswa/<?php echo $value->color_id; ?>/delete">
                          <button type="button" class="btn btn-xs btn-danger btn-flat" onclick="ConfirmDelete()"><i class="fa fa-trash"></i> Delete</button>
                          </a>
                          <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-edit"></i> Edit</button>
                      </td>
                    </tr>

                    <?php

                    }

                    ?>

                  </tbody></table>
                </div><!-- /.box-body -->
              </div>
          </div>


          <!-- Your Page Content Here -->
          </div>
          


        </section>
