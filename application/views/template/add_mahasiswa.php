
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Input Mahasiswa
            <small>Optional description</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">


          <div class="col-md-6">
            <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">Form data mahasiswa</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <?php echo form_open('admin/store', ['class' => 'form-signin', 'role' => 'form']); ?>

                      <!-- text input -->
                      <div class="form-group">
                        <label>NPM</label>
                        <input type="text" class="form-control" placeholder="nomor pokok mahasiswa" name="npm">
                      </div>
                      <div class="form-group">
                        <label>Nama Mahasiswa</label>
                        <input type="text" class="form-control" placeholder="nama mahasiswa" name="nm_mhs">
                      </div>

                      <div class="form-group">
                          <label>Kelas</label>
                          <select class="form-control" name="kelas">
                            <option value="none">-- Pilih kelas --</option>
                            <option>Reguler A</option>
                            <option>Reguler B</option>
                            <option>Reguler C</option>
                            <option>Karyawan A</option>
                            <option>Karyawan B</option>
                          </select>
                      </div>

                      <div class="form-group">
                          <label>Semester</label>
                          <select class="form-control" name="semester">
                            <option value="none">-- Pilih Semester --</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                          </select>
                      </div>

                      <div class="form-group">
                          <label>Peminatan</label>
                          <select class="form-control" name="peminatan">
                            <option value="none">-- Pilih Peminatan --</option>
                            <option>Rekayasa Perangkat Lunak</option>
                            <option>Sistem Informasi</option>
                            <option>Network Centric Center</option>
                            <option>Geologi Informatia</option>

                          </select>
                      </div>
                      <div class="form-group">
                        <button class="btn btn-block btn-default">Reset</button>
                        <button class="btn btn-block btn-success">Save</button>
                      </div>

                    </form>
                    <?php echo form_close(); ?>
                  </div><!-- /.box-body -->
                </div>
          </div>


          <!-- Your Page Content Here -->
          </div>


        </section><!-- /.content -->
