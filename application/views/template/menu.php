      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url(); ?>public/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>
                <?php
                  echo $this->session->userdata('username');
                ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">HEADER</li>

            <?php
              if($this->session->userdata('is_role_user') == 2112){
              ?>

              <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Request Order</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="/admin/ro"><i class="fa fa-circle-o"></i> Request</a></li>
                  <li><a href="/admin/ro/approved_by"><i class="fa fa-circle-o"></i> Approved Manager</a></li>
                </ul>
              </li>

            <?php
            }elseif($this->session->userdata('is_role_user') == 2114) {


            ?>
              <li class="active"><a href="/admin/ro"><i class="fa fa-link"></i> <span> Request Order (Designer)</span></a></li>
            <?php
          }elseif($this->session->userdata('is_role_user') == 2113){

            ?>
            <li class="active"><a href="/admin/ro"><i class="fa fa-link"></i> <span> Request Order (Manager)</span></a></li>
            <?php
            }else {

            ?>
            <li class="active"><a href="/admin/ro"><i class="fa fa-link"></i> <span> Request Order</span></a></li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="/admin/deliveryorder"><i class="fa fa-link"></i> <span> Delivery Order</span></a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-cart-plus"></i> <span>Purchase Order</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="/admin/po"><i class="fa fa-circle-o"></i> Request Approved</a></li>
                <li><a href="/admin/purchase"><i class="fa fa-circle-o"></i> Purchase</a></li>
              </ul>
            </li>

            <li class="active"><a href="/admin/supplier"><i class="fa fa-link"></i> <span> Supplier</span></a></li>
            <li class="active"><a href="/admin/company"><i class="fa  fa-institution"></i> <span> Company</span></a></li>

            <li class="treeview">
              <a href="#"><i class="fa fa-link"></i> <span>Product</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="/admin/material"><i class="fa fa-circle-o"></i> Material</a></li>
                <li><a href="/admin/color"><i class="fa fa-circle-o"></i> Color</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#"><i class="fa fa-users"></i> <span>User</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="/admin/roleuser"><i class="fa fa-circle-o"></i> Role User</a></li>
                <li><a href="/admin/listuser"><i class="fa fa-circle-o"></i> All User</a></li>
              </ul>
            </li>
          </ul><!-- /.sidebar-menu -->
          <?php
            }
          ?>
        </section>
        <!-- /.sidebar -->
      </aside>
