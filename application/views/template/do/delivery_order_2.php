<section class="content">
          <div class="row">


          <div class="col-md-12">

            <?php

            if ($this->session->flashdata('flsh_msg')!=null) {

             ?>
                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4>	<i class="icon fa fa-check"></i> Berhasil!</h4>
                                <?php echo $this->session->flashdata('flsh_msg'); ?>
                              </div>
            <?php
            }
            ?>

            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Delivery Order</h3>
                  <div class="box-tools">
                      <a href="/admin/mahasiswa/add">
                        <button class="btn btn-sm btn-block btn-success"><i class="fa fa-plus fa-align-left"> </i>  Create PO</button>
                      </a>

                    </div>
                </div><!-- /.box-header -->


                <div class="box-body no-padding">

                  <?php //var_dump($data_mhs); ?>

                  <table class="table table-striped">
                    <tbody><tr>
                      <th style="width: 10px">#</th>
                      <th>No PO</th>
                      <th>Material</th>
                      <th>Date Created</th>

                      <th>Qty</th>
                      <th>Price</th>
                      <th>Jumlah</th>
                      <th>Status</th>
                      <th>Action</th>

                    </tr>

                    <?php
                      $no = 1;
                      //var_dump($data_list_user);
                      foreach ($data_list_po as $key => $value) {
                    ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $value->no_po; ?></td>
                      <td><?php echo $value->material_name; ?></td>
                      <td><?php echo $value->date_created; ?></td>
                      <td><?php echo $value->qty; ?></td>
                      <td><?php echo $value->price; ?></td>
                      <td><?php echo $value->total; ?></td>
                      <td>
                        <?php
                        if($value->status == 0){
                          echo "pending manager";
                        }elseif($value->status == 1){
                          echo "pending direktur";
                        }else {
                          echo "approved";
                        }


                       ?></td>


                      <td>

                          <a href="mahasiswa/<?php echo $value->id; ?>">
                            <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-list-alt"></i> Detail</button>
                          </a>
                          <a href="mahasiswa/<?php echo $value->id; ?>/delete">
                          <button type="button" class="btn btn-xs btn-danger btn-flat" onclick="ConfirmDelete()"><i class="fa fa-trash"></i> Delete</button>
                          </a>
                          <a href="mahasiswa/<?php echo $value->id; ?>/edit">
                          <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-edit"></i> Edit</button>
                          </a>
                      </td>
                    </tr>

                    <?php

                    }

                    ?>

                  </tbody></table>
                </div><!-- /.box-body -->
              </div>
          </div>


          <!-- Your Page Content Here -->
          </div>



        </section>
