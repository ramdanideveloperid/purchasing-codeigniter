<section class="content">
          <div class="row">


          <div class="col-md-12">

            <?php

            if ($this->session->flashdata('flsh_msg')!=null) {

             ?>
                              <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4>	<i class="icon fa fa-check"></i> Berhasil!</h4>
                                <?php echo $this->session->flashdata('flsh_msg'); ?>
                              </div>
            <?php
            }
            ?>

            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">List All User</h3>
                  <div class="box-tools">
                      <a href="/admin/user/new_user">
                        <button class="btn btn-sm btn-block btn-success"><i class="fa fa-plus fa-align-left"> </i>  Add New User</button>
                      </a>
                    </div>
                </div><!-- /.box-header -->


                <div class="box-body no-padding">

                  <?php //var_dump($data_mhs); ?>

                  <table class="table table-striped">
                    <tbody><tr>
                      <th style="width: 10px">#</th>
                      <th>Username</th>
                      <th>Role</th>
                      <th>Email</th>
                      <th>Created</th>
                      <th>Is Admin</th>

                      <th>Action</th>

                    </tr>

                    <?php
                      $no = 1;
                      //var_dump($data_list_user);
                      foreach ($data_list_user as $key => $value) {
                    ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $value->username; ?></td>
                      <td><?php echo $value->nm_role_user; ?></td>
                      <td><?php echo $value->email; ?></td>
                      <td><?php echo $value->created_at; ?></td>
                      <td>
                        <?php
                        if($value->is_admin == 0){
                          echo "User";
                        }else {
                          echo "Admin";
                        }


                       ?></td>


                      <td>
                          <a href="#">
                            <button type="button" class="btn btn-xs btn-success btn-flat"><i class="fa fa-list-alt"></i> View</button>
                          </a>
                          <a href="#">
                          <button type="button" class="btn btn-xs btn-danger btn-flat" onclick="ConfirmDelete()"><i class="fa fa-trash"></i> Delete</button>
                          </a>
                          <a href="#">
                          <button type="button" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-edit"></i> Edit</button>
                          </a>
                      </td>
                    </tr>

                    <?php

                    }

                    ?>

                  </tbody></table>
                </div><!-- /.box-body -->
              </div>
          </div>


          <!-- Your Page Content Here -->
          </div>



        </section>
