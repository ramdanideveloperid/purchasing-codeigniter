<?php

/**
 *
 */
class Mahasiswa_model extends CI_Model
{

  function __construct()
  {
      parent::__construct();
  }

  public function getDataMahasiswa()
  {

    //$query = $this->db->query("SELECT * FROM mahasiswa");
    $query = $this->db->get('warna');
    return $query->result();

  }

  public function getDetailMahasiswa($value='')
  {
    $query = $this->db->select('*')
                ->where('npm', $value)
                ->get('mahasiswa');
    return $query->result();
  }

  public function insertMahasiswa($npm,$nm_mhs,$kelas,$semester,$peminatan)
  {
    $data = array(
            'npm' => $npm,
            'nama_mhs' => $nm_mhs,
            'kelas' => $kelas,
            'semester' => $semester,
            'peminatan' => $peminatan
    );

    return $this->db->insert('mahasiswa', $data);

  }

  public function deleteMahasiswa($value='')
  {
    return $this->db->delete('purchase_order', array('po_id' => $value));
  }


}
