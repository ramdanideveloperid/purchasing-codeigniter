<?php

class Company extends CI_Model
{

  function __construct()
  {
      parent::__construct();
  }

  public function getCompany()
  {

    $query = $this->db->get('company');
    return $query->result();

  }
}
