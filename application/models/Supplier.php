<?php

class Supplier extends CI_Model
{

  function __construct()
  {
      parent::__construct();
  }

  public function getMaterialSupplier()
  {

        $query = $this->db->query("SELECT
          material.*,color.*, supplier.*
          FROM material, color, supplier
          WHERE material.color_id = color.color_id
          AND supplier.material_id = material.material_id
          ");

        return $query->result();

  }

}
