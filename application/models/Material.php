<?php

class Material extends CI_Model
{

  function __construct()
  {
      parent::__construct();
  }

    public function getColor()
    {

      //$query = $this->db->query("SELECT * FROM mahasiswa");
      $query = $this->db->get('color');
      return $query->result();

    }

    public function getMaterial()
    {

          $query = $this->db->query("SELECT material.*,color.* FROM material, color WHERE material.color_id = color.color_id");

          return $query->result();

    }

    public function getPriceMaterial($getMaterialId)
    {

          $query = $this->db->query("SELECT
            material.*,color.*, supplier.*
            FROM material, color, supplier
            WHERE material.color_id = color.color_id
            AND supplier.material_id = material.material_id
            AND supplier.supplier_id = ".$getMaterialId."");

          return $query->result();

    }

  }
