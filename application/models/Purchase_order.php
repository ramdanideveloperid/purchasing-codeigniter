<?php

class Purchase_order extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getListPurchaseOrder()
    {

          $query = $this->db->query("SELECT
            purchase_order.*,supplier.*,material.* FROM
            purchase_order, supplier,material WHERE
            purchase_order.supplier_id = supplier.supplier_id AND supplier.material_id = material.material_id AND purchase_order.status = 2 ORDER BY date_created DESC");

          return $query->result();

    }
  }
