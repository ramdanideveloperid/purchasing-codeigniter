<?php

class Request_order extends CI_Model
{

  function __construct()
  {
      parent::__construct();
  }


  public function insertRequest($noro,$date,$company,$qty,$total,$supplier_id)
  {
    $data = array(
            'no_ro' => $noro,
            'date_created' => $date,
            'company_id' => $company,
            'supplier_id' => $supplier_id,
            'qty' => $qty,
            'total' => $total
    );

    return $this->db->insert('purchase_order', $data);

  }

  public function updateRequestManager($id)
  {
      return $this->db->query("UPDATE purchase_order SET status = '1' WHERE no_ro = '".$id."'");
  }

  public function approvement($value='')
  {
    $query = $this->db->set('status', '1', FALSE);
    $query = $this->db->where('po_id', $value);
    $query = $this->db->update('purchase_order');

    return $query;

  }

  public function approvement_by_direktur($value='')
  {
    $query = $this->db->set('status', '2', FALSE);
    $query = $this->db->where('po_id', $value);
    $query = $this->db->update('purchase_order');

    return $query;

  }

  public function getDetailRo($value='')
  {
    $query = $this->db->query("SELECT
      purchase_order.*,supplier.*,material.*, company.* FROM
      purchase_order, supplier,material, company WHERE
      purchase_order.supplier_id = supplier.supplier_id
      AND supplier.material_id = material.material_id
      AND purchase_order.company_id = company.company_id
      AND purchase_order.po_id =".$value);
    return $query->result();
  }

  public function getRequest()
  {

        $query = $this->db->query("SELECT
          purchase_order.*,supplier.*,material.*, company.* FROM
          purchase_order, supplier,material, company WHERE
          purchase_order.supplier_id = supplier.supplier_id
          AND supplier.material_id = material.material_id
          AND purchase_order.company_id = company.company_id
          AND purchase_order.status = 0 ORDER BY date_created DESC");

        return $query->result();

  }

  public function getRequestAll()
  {

        $query = $this->db->query("SELECT
          purchase_order.*,supplier.*,material.*, company.* FROM
          purchase_order, supplier,material, company WHERE
          purchase_order.supplier_id = supplier.supplier_id
          AND supplier.material_id = material.material_id
          AND purchase_order.company_id = company.company_id ORDER BY date_created DESC");

        return $query->result();

  }

  public function getPurchased()
  {

        $query = $this->db->query("SELECT
          purchase_order.*,supplier.*,material.*, company.* FROM
          purchase_order, supplier,material, company WHERE
          purchase_order.supplier_id = supplier.supplier_id
          AND supplier.material_id = material.material_id
          AND purchase_order.company_id = company.company_id
          AND purchase_order.status = 3 ORDER BY date_created DESC");

        return $query->result();

  }




  public function getROApproveManager()
  {

        $query = $this->db->query("SELECT
          purchase_order.*,supplier.*,material.*, company.* FROM
          purchase_order, supplier,material, company WHERE
          purchase_order.supplier_id = supplier.supplier_id
          AND supplier.material_id = material.material_id
          AND purchase_order.company_id = company.company_id
          AND purchase_order.status = 1 ORDER BY date_created DESC");

        return $query->result();

  }


}
