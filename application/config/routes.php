<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin';

$route['admin/mahasiswa'] = 'mahasiswa/index';
$route['admin/mahasiswa/add'] = 'mahasiswa/add';
$route['admin/mahasiswa/save'] = 'mahasiswa/store';
$route['admin/mahasiswa/(:num)'] = 'mahasiswa/detail/$1';
$route['admin/mahasiswa/(:num)/delete'] = 'mahasiswa/delete/$1';

$route['admin/ro'] = 'admin/ro';
$route['admin/ro/approved_by'] = 'ro/approved_by_manager';
$route['admin/ro/new_request'] = 'ro/new_request';
$route['admin/ro/save'] = 'ro/store';
$route['admin/ro/(:num)'] = 'ro/detail/$1';
$route['admin/ro/(:num)/createpo'] = 'ro/createpo/$1';
$route['admin/ro/(:num)/delete'] = 'ro/delete/$1';
//$route['admin/ro/(:num)/approve'] = 'ro/approve/$1';
$route['admin/ro/(:num)/approvem'] = 'ro/approve_manager/$1';
$route['admin/ro/(:num)/approved'] = 'ro/approve_direktur/$1';

$route['admin/listuser'] = 'admin/listuser';
$route['admin/user/new_user'] = 'user/register';
$route['admin/user/save'] = 'user/store';
$route['admin/user/(:num)'] = 'user/detail/$1';
$route['admin/user/(:num)/delete'] = 'user/delete/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
