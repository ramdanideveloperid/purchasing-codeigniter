<?php

/**
 *
 */
class Admin extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('template');
    $this->load->helper('url');

    //model
    $this->load->model('Mahasiswa_model');
    $this->load->model('User_model');
    $this->load->model('Material');
    $this->load->model('Company');
    $this->load->model('Purchase_order');
    $this->load->model('Request_order');

    $this->load->library(array('session'));
    $this->load->helper(array('url'));
    $this->load->model('user_model');
  }

  public function index()
  {
    		// create the data object
    		$data = new stdClass();

        if ($this->is_logged_in('logged_in') == 1) {
          // user login ok

          $data = array();
          $data['cobaa'] = $this->user_model->getRoleName($this->is_logged_in('is_role_user'));
          $this->template->display('welcome_message',$data);

        }else {


            redirect('admin/login');

        }




  }

  public function material()
  {
    $data = array();
    $data['data_material'] = $this->Material->getMaterial();
    //var_dump($data);
    //var_dump( (array) $data);
    //echo json_encode($data);
    $this->template->display('template/material/material',$data);
  }

  public function company()
  {
    $data = array();
    $data['data_company'] = $this->Company->getCompany();
    //var_dump($data);
    //var_dump( (array) $data);
    //echo json_encode($data);
    $this->template->display('template/company/company',$data);
  }

  public function supplier()
  {
    $data = array();
    $data['data_company'] = $this->Company->getCompany();
    //var_dump($data);
    //var_dump( (array) $data);
    //echo json_encode($data);
    $this->template->display('template/company/company',$data);
  }

  public function is_logged_in($values)
  {
          $user = $this->session->userdata($values);
          return isset($user);
  }

  public function coba()
  {
    $data = array(

            'title'     =>   'Hello World!',
            'content'   =>   'This is the content',
            'posts'     =>   array('Post 1', 'Post 2', 'Post 3')

    );

    $this->load->view('hello_world', $data);
    /*
    $data = array(

    'title' => 'Title goes here',

    );

    $this->load->library('template');
    $this->template->load('default', 'content', $data);
    */
    $data = array(

    'title' => 'Title goes here',

    );
    $this->twig->display('welcome', $data);
  }

  public function mantap()
  {
      $this->template->display('welcome_message');
  }
  public function other()
  {
      $data = array(

      'title' => 'Title goes here',

      );
      //$this->template->display('hello_world',$data);
      $this->template->display('template/add_mahasiswa',$data);
  }
  public function mahasiswa()
  {
      $data = array();
      $data['data_mhs'] = $this->Mahasiswa_model->getDataMahasiswa();
      //var_dump( (array) $data);
      //echo json_encode($data);
      $this->template->display('template/color',$data);
  }

  public function color()
  {
      $data = array();
      $data['data_warna'] = $this->Material->getColor();
      //var_dump( (array) $data);
      //echo json_encode($data);
      $this->template->display('template/color',$data);
  }

  public function roleuser()
  {
      $data = array();
      $data['data_role_user'] = $this->User_model->getRoleUser();
      //var_dump( (array) $data);
      //echo json_encode($data);
      $this->template->display('template/user/role_user',$data);
  }

  public function listuser()
  {
      $data = array();
      $data['data_list_user'] = $this->User_model->getListAllUser();
      //var_dump( (array) $data);
      //echo json_encode($data);
      $this->template->display('template/user/list_user',$data);
  }

  public function po()
  {
    // create the data object
    $data = new stdClass();
      //$data = array();
      //$data['data_mhs'] = $this->Mahasiswa_model->getDataMahasiswa();
      //var_dump( (array) $data);
      //echo json_encode($data);
      if ($this->is_logged_in('logged_in') == 1) {
        // user login ok
        $data = array();
        $data['data_list_po'] = $this->Purchase_order->getListPurchaseOrder();
        //var_dump($data);
        $this->template->display('template/po/purchase_order',$data);

      }else {

          //$this->load->view('template/user/login/login', $data);
          redirect('admin/login');
      }

  }

  public function deliveryorder()
  {
    // create the data object
    $data = new stdClass();

      if ($this->is_logged_in('logged_in') == 1) {
        // user login ok
        $data = array();
        $data['data_list_po'] = $this->Purchase_order->getListPurchaseOrder();
        $this->template->display('template/do/delivery_order',$data);

      }else {

          redirect('admin/login');
      }

  }

  public function ro()
  {
      $data = array();
      if($this->session->userdata('is_role_user') == 2113){
        $data['data_list_po'] = $this->Request_order->getROApproveManager();
      }elseif($this->session->userdata('is_role_user') == 2114){
        $data['data_list_po'] = $this->Request_order->getRequest();
      }elseif($this->session->userdata('is_role_user') == 2112){
        $data['data_list_po'] = $this->Request_order->getRequest();
      }elseif($this->session->userdata('is_role_user') == 2222){
          $data['data_list_po'] = $this->Request_order->getRequestAll();
      }

      $this->template->display('template/ro/request_order',$data);
  }

  public function purchase()
  {

    if($this->session->userdata('is_role_user') == 2222){
        $data['data_list_po'] = $this->Request_order->getPurchased();
    }

    $this->template->display('template/ro/request_order',$data);
  }



  public function login_page()
  {

      $this->load->view('template/login',null);
  }



  public function add()
  {
    $this->template->display('template/add_mahasiswa',null);
  }

  public function detail($value='')
  {
    //echo "rizki aja".$value;
    $data = array();
    $data['detail'] = $this->Mahasiswa_model->getDetailMahasiswa($value);

    echo json_encode($data);
  }

  public function store()
  {
    $getNPM = $this->input->post('npm');
    $getNamaMhs = $this->input->post('nm_mhs');
    $getKelasMhs = $this->input->post('kelas');
    $getSemester = $this->input->post('semester');
    $getPeminatan = $this->input->post('peminatan');

    $data = $this->Mahasiswa_model->insertMahasiswa($getNPM,$getNamaMhs,$getKelasMhs,$getSemester,$getPeminatan);

    if ($data== true) {
      $this->session->set_flashdata('flsh_msg', 'data berhasil ditambahkan');
      redirect('admin/mahasiswa');
    }

  }

  /**
   * login function.
   *
   * @access public
   * @return void
   */
  public function login() {

    // create the data object
    $data = new stdClass();

    // load form helper and validation library
    $this->load->helper('form');
    $this->load->library('form_validation');

    // set validation rules
    $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
    $this->form_validation->set_rules('password', 'Password', 'required');

    if ($this->form_validation->run() == false) {

      // validation not ok, send validation errors to the view

      if ($this->is_logged_in('logged_in') == 1) {
        // user login ok

          $this->template->display('welcome_message');

      }else {

          $this->load->view('template/login');

      }


    } else {

      // set variables from the form
      $username = $this->input->post('username');
      $password = $this->input->post('password');

      if ($this->user_model->resolve_user_login($username, $password)) {

        $user_id = $this->user_model->get_user_id_from_username($username);

        $user = $this->user_model->get_user($user_id);

        //$user = $this->User_model->getRoleName();


        // set session user datas
        $_SESSION['user_id']      = (int)$user->id;
        $_SESSION['username']     = (string)$user->username;
        $_SESSION['logged_in']    = (bool)true;
        $_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
        $_SESSION['is_admin']     = (bool)$user->is_admin;
        $_SESSION['is_role_user'] = (int)$user->role_id;

        $user = $this->user_model->get_role_name($this->session->userdata('is_role_user'));
        $_SESSION['is_role_name'] = (string)$user->nm_role_user;


        //$_SESSION['is_role_name'] = (string)$user->role_name;

        // user login ok

        $coba['waw'] = $user;

        redirect('/admin');



      } else {

        // login failed
        $data->error = 'Wrong username or password.';

        // send error to the view

        $this->load->view('template/login', $data);


      }

    }

  }

  /**
   * logout function.
   *
   * @access public
   * @return void
   */
  public function logout() {

    // create the data object
    $data = new stdClass();

    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {

      // remove session datas
      foreach ($_SESSION as $key => $value) {
        unset($_SESSION[$key]);
      }

      // user logout ok

      $this->load->view('template/user/logout/logout_success', $data);

      redirect('admin');

    } else {

      // there user was not logged in, we cannot logged him out,
      // redirect him to site root
      redirect('/');

    }

  }
}
