<?php

/**
 *
 */
class Mahasiswa extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('template');
    $this->load->model('Mahasiswa_model');
  }

  public function index()
  {
    $data = array();
    $data['data_mhs'] = $this->Mahasiswa_model->getDataMahasiswa();
    $this->template->display('template/data_mahasiswa',$data);
  }

  public function add()
  {
      $this->template->display('template/add_mahasiswa',null);
  }

  public function detail($value='')
  {
      $data = array();
      $data['detail'] = $this->Mahasiswa_model->getDetailMahasiswa($value);
      echo json_encode($data);
  }

  public function delete($value='')
  {
      $data = $this->Mahasiswa_model->deleteMahasiswa($value);
      if ($data== true) {
        $this->session->set_flashdata('flsh_msg', 'data berhasil dihapus');
        redirect('admin/mahasiswa');
      }
  }


  public function store()
  {
      $getNPM = $this->input->post('npm');
      $getNamaMhs = $this->input->post('nm_mhs');
      $getKelasMhs = $this->input->post('kelas');
      $getSemester = $this->input->post('semester');
      $getPeminatan = $this->input->post('peminatan');

      $data = $this->Mahasiswa_model->insertMahasiswa($getNPM,$getNamaMhs,$getKelasMhs,$getSemester,$getPeminatan);

      if ($data== true) {
        $this->session->set_flashdata('flsh_msg', 'data berhasil ditambahkan');
        redirect('admin/mahasiswa');
      }
  }

  }
