<?php

/**
 *
 */
class Ro extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('template');
    $this->load->model('Material');
    $this->load->model('Request_order');
    $this->load->model('Mahasiswa_model');
    $this->load->model('Supplier');
    $this->load->model('Company');
  }

  public function new_request()
  {
      $data = array();
      //$data['data_material'] = $this->Material->getMaterial();
      $data['data_material_supplier'] = $this->Supplier->getMaterialSupplier();
      $data['data_company'] = $this->Company->getCompany();

      $this->template->display('template/ro/new_request',$data);
  }
  public function store()
  {
      $getNoRO = $this->input->post('no_ro');
      $getSupplier = $this->input->post('material');
      $getDateCreated = $this->input->post('date_created');
      $getCompanyName = $this->input->post('company_id');
      $getQty = $this->input->post('qty');

      echo  $getSupplier;

      $data = array();
      $data = $this->Material->getPriceMaterial($getSupplier);

      //var_dump($data);

       foreach ($data as $key => $value) {
          $harga = $value->price;
       }
      //
       $total = $harga*$getQty;
      //
      // echo $total;

      $data = $this->Request_order->insertRequest($getNoRO,$getDateCreated,$getCompanyName,$getQty, $total,$getSupplier);

      if ($data== true) {
        //$this->session->set_flashdata('flsh_msg', 'data berhasil ditambahkan');
        redirect('admin/ro');
      }
  }
  public function approve_manager($value='')
  {
    $data = $this->Request_order->approvement($value);
    if ($data== true) {
      $this->session->set_flashdata('flsh_msg', 'Request Order berhasil di approve oleh manager');
      redirect('admin/ro');
    }

  }

  public function approve_direktur($value='')
  {
    $data = $this->Request_order->approvement_by_direktur($value);
    if ($data== true) {
      $this->session->set_flashdata('flsh_msg', 'Request Order berhasil di approve oleh direktur');
      redirect('admin/ro');
    }

  }

  public function detail($value='')
  {
      $data = array();
      $data['detail_ro'] = $this->Request_order->getDetailRo($value);
      //echo json_encode($data);
      //var_dump($data);
      $this->template->display('template/ro/view_request_order',$data);
  }

  public function createpo($value='')
  {
      $data = array();
      $data['detail_ro'] = $this->Request_order->getDetailRo($value);
      //echo json_encode($data);
      //var_dump($data);
      $this->template->display('template/ro/create_po',$data);
  }

  public function delete($value='')
  {
      $data = $this->Mahasiswa_model->deleteMahasiswa($value);
      if ($data== true) {
        $this->session->set_flashdata('flsh_msg', 'data berhasil dihapus');
        redirect('admin/ro');
      }
  }

  public function approved_by_manager()
  {
      $data = array();
      $data['data_list_po'] = $this->Request_order->getROApproveManager();
      $this->template->display('template/ro/request_order',$data);
  }

}
